<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class MovisListTest extends TestCase
{
    /**
     * test get All movis.
     */
    public function test_All_movis_successful_response()
    {
        $this->actingAs($user = User::factory()->create());
        Http::fake(
            [
                'https://api.themoviedb.org/3/trending/movie/day' => $this->fakePopularMovies(),
            ]
        );
        $response = $this->get(route('movies.index'));
        $response->assertSuccessful();
    }

    private function fakePopularMovies()
    {
        return Http::response(
            [
                'results' => [
                    [
                        "id" => 419704,
                        "adult" => false,
                        "original_language" => "en",
                        "original_title" => "Fake Movie",
                        "poster_path" => "/path/to/poster1.jpg",
                        "title" => "Fake Movie",
                    ],
                    [
                        "id" => 419705,
                        "adult" => false,
                        "original_language" => "en",
                        "original_title" => "Fake Movie 2",
                        "poster_path" => "/path/to/poster2.jpg",
                        "title" => "Fake Movie 2",
                    ]
                ]
            ],
            200 );
    }
}
