# Watchtower-back
![version](https://img.shields.io/badge/Mysql-8.0-orange)
![version](https://img.shields.io/badge/PHP-8.1-red)
![version](https://img.shields.io/badge/Sail-8.2-important)
![version](https://img.shields.io/badge/laravel-10-important)

## Contenus
+ [Prérequis](#Prérequis)
+ [Installation](#installation)
+ [Utilisation](#Utilisation)
+ [Images](#images)
+ [Laravel](#laravel)


# Prérequis
+ [Docker](https://www.docker.com/get-started/) s'execute sur la machine hôte.
+ Docker compose s'execute sur la machine hôte.
+ Connaissance de base de Docker.

# Installation
+ Pour commencer, il faut suivre les étapes suivantes :
+ Clonez le dépôt à partir de [Gitlab](https://gitlab.com/badrCHRIFI/movies_app.git).
+ Créez un fichier .env et copiez-y le contenu .env.example
+ Exécuter `vendor/bin/sail up`
+ Exécuter `Sail up -d` or `docker-compose start`
+ Exécuter `docker exec -it movis-app_laravel.test_1 bash composer install`
+ Exécuter `docker exec -it movis-app_laravel.test_1 artisan migrate:fresh`
+ Visitez http://localhost:80 pour voir votre application Laravel

# Images
+ laravel.test
+ mysql
+ redis
+ meilisearch
+ mailpit
+ selenium

# Laravel
+ 10 version
