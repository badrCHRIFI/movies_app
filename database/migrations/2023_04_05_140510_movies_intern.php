<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('movies_intern', function (Blueprint $table) {
            $table->id();
            $table->string( 'title', 255 )->nullable();
            $table->string( 'original_language', 255 )->nullable();
            $table->text( 'overview' )->nullable();
            $table->date( 'release_date' )->nullable();
            $table->string( 'vote_average', 255 )->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('movies_intern');
    }
};
