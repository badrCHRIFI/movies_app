<?php

namespace App\Http\Controllers;
use App\Services\MoviesServies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class MoviesController
{
    public function __construct(private MoviesServies $MoviesServies) {}

/**
     * Display a listing of the resource.
     */
    public function index()
    {
        $popularMovies = $this->MoviesServies->getMoviesFromApi();

        return view('movies.index', [
            'popularMovies' => $popularMovies,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $movie = Http::withToken(config('services.mvdb.token'))
            ->get('https://api.themoviedb.org/3/movie/'.$id.'?append_to_response=credits,videos,images')
            ->json();

        return view('movies.show', [
            'movie' => $movie,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
