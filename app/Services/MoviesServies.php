<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class MoviesServies
{
    private string $apiThemoviedbUrl;
    private string $apiThemoviedbToken;

    public function __construct()
    {
        $this->apiThemoviedbUrl = 'https://api.themoviedb.org/3/trending/movie/day';
        $this->apiThemoviedbToken = config('services.mvdb.token');
    }

    public function getMoviesFromApi(): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|array|\Illuminate\Contracts\Foundation\Application
    {
        try {
            $popularMovies = Http::withToken(config('services.mvdb.token'))
                ->get("$this->apiThemoviedbUrl");
            return $popularMovies->json()['results'];

        } catch (\Exception $e) {
            Log::error('Error getting Movie: ' . $e->getMessage());
            return [];
        }
    }

}
