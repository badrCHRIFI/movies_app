<?php

namespace App\Console\Commands;

use App\Models\Movies;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Schema;

class ImportMovies extends Command
{
    /**
     * importation de movies.
     *
     * @var string
     */
    protected $signature = 'import-movies';

    /**
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {

        $client = new Client();
        $popularMovies = 'https://api.themoviedb.org/3/trending/movie/day';
        $headers=['Authorization'=>'Bearer '.config('services.mvdb.token')];
        $response = $client->request('GET', $popularMovies, ['headers'=>$headers]);
        $movies = json_decode($response->getBody());
        foreach ($movies->results as $movie)
        {
            Movies::create([
                              'title'=>$movie->title ?? null,
                              'original_language'=>$movie->original_language ?? null,
                              'overview'=>$movie->overview ?? null,
                              'vote_average'=>$movie->vote_average ?? null,
                              'release_date'=>$movie->release_date ?? null,
                              'vote_count'=>$movie->vote_count ?? null
                          ]);
        }
        $this->info("Importation terminé");
    }
}
