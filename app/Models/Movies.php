<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movies extends Model
{
    use HasFactory;
    protected $table = 'movies_intern';
    protected $fillable = [
        'Id',
        'title',
        'original_language',
        'overview',
        'release_date',
        'vote_average',
    ];
}
