<x-app-layout>
    <div class="container mx-auto px-4 pt-6">
            <h1 class="uppercase tracking-wider text-orange-500 text-2xl font-semibold">{{ $movie['original_title'] }}</h1>
                    <div class="mt-8 flex flex-row">
                        <div class="basis-1/2">
                            <img src="{{ 'https://image.tmdb.org/t/p/w780/'.$movie['poster_path'] }}" alt="poster" class="hover:opacity-75 transition ease-in-out duration-150">
                        </div>
                        <div class="mt-16 p-6 basis-1/2">
                            <h3 class="text-2xl mt-2 hover:text-gray-300">Title : {{ $movie['original_title'] }}</h3>
                            <h4 class="text-2xl mt-2 hover:text-gray-300">Original Language : {{ $movie['original_language'] }}</h4>
                            <h4 class="text-2xl mt-2 hover:text-gray-300">Description : {{ $movie['overview'] }}</h4>
                            <h4 class="text-2xl mt-2 hover:text-gray-300">Release Date : {{ $movie['release_date'] }}</h4>
                            <h4 class="text-2xl mt-2 hover:text-gray-300">Vote Average : {{ $movie['vote_average'] }}</h4>
                            <div class="pt-6">
                                <h4 class="text-2xl mt-2 hover:text-gray-300">crew : </h4>
                            @foreach ($movie['credits']['crew'] as $crew)
                                    @if($loop->index < 4)
                                        <div class="mr-8 pt-6">
                                            <div>{{ $crew['name'] }}</div>
                                            <div class="text-sm text-gray-400">{{ $crew['job'] }}</div>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                            <div x-data="{ isOpen: false }" class="pt-6">
                                @if (count($movie['videos']['results']) > 0)
                                    <div class="mt-12">
                                        <a
                                            href="https://www.youtube.com/watch?v={{ $movie['videos']['results'][0]['key'] }}"
                                            class="flex inline-flex items-center bg-orange-500 text-gray-900 rounded font-semibold px-5 py-4 hover:bg-orange-600 transition ease-in-out duration-150"
                                        >
                                            <svg class="w-6 fill-current" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M10 16.5l6-4.5-6-4.5v9zM12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"/></svg>
                                            <span class="ml-2">Play Trailer</span>
                                        </a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
        <div class="container mx-auto px-4 py-16">
            <h2 class="text-2xl font-semibold pt-6">Cast</h2>
            <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-8">
                @foreach ($movie['credits']['cast'] as $cast)
                    @if($loop->index < 12)
                    <div class="mt-8">
                        <img src="{{  'https://image.tmdb.org/t/p/w185/'.$cast['profile_path'] }}" alt="actor1" class="hover:opacity-75 transition ease-in-out duration-150">
                        <div class="mt-2">
                            <h4 class="text-2xl mt-2 hover:text-gray:300">{{ $cast['name'] }}</h4>
                            <div class="text-xl text-gray-400">
                                {{ $cast['character'] }}
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
    </div>
</x-app-layout>
