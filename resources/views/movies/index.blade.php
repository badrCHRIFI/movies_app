<x-app-layout>
    <div class="py-12">
            <div class=" max-w-7xl mx-auto sm:px-6 lg:px-8">
                <h2 class="uppercase tracking-wider text-orange-500 text-2xl font-semibold">Popular Movies</h2>
                <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-5 gap-8">
                    @foreach ($popularMovies as $movie)
                        <div class="mt-8">
                            <div class="mt-2">
                                <a href="{{ route('movies.show', $movie['id']) }}">
                                    <img src="{{ 'https://image.tmdb.org/t/p/w300/'.$movie['poster_path'] }}" alt="poster" class="hover:opacity-75 transition ease-in-out duration-150">
                                </a>
                                <a href="{{ route('movies.show', $movie['id']) }}" class="text-lg mt-2 hover:text-gray-300">
                                    <h3 class="text-xl mt-2 hover:text-gray-300">{{ $movie['title'] }}</h3>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
    </div>
</x-app-layout>
